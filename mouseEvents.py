import mouse 

class MouseEvents:

    debug  = None
    
    def __init__(self):
        self.debug = "LOL"

 
        #Fenetre du jeu : haut a gauche : 309 / 110 | en bas  droite  1743 - 949

        #Definition des positions de la souris en fonction de la zone de jeu
        #PlayButton :  1279 - 789 

      
    def playButton(self, message):
        playButton = [1298, 810]

        mouse.move(playButton[0], playButton[1], True, 2)
        mouse.click(button="left")
        
        

    def muligan(self, message):

     ################ MULLIGAN ################
        
        #Muligan Positions
        MU1 = [730, 519]
        MU2 = [988, 524]
        MU3 = [1250, 517]
        CMU = [980, 789] 

        if(message == "mu1"):
            mouse.move(MU1[0], MU1[1], True, 3)
            mouse.click(button="left")

        if(message == "mu2"):
            mouse.move(MU2[0], MU2[1], True, 3)
            mouse.click(button="left")

        if(message == "mu3"):
            mouse.move(MU3[0], MU3[1], True, 3)
            mouse.click(button="left")

        if(message == "cmu"):
            mouse.move(CMU[0], CMU[1], True, 3)
            mouse.click(button="left")
        
     ################ MULLIGAN ################
   

    def boardPlay(self, message):

        ################ BOARD  ################

        #HeroPower
        HP = [1095, 754]

        #Ally
        Ally = []
        B0 = [650, 588]
        B1 = [750, 588]
        B2 = [850, 588]
        B3 = [950, 588]
        B4 = [1050, 588]
        B5 = [1150, 588]
        B6 = [1250, 588]

        Ally.append(B0)
        Ally.append(B1)
        Ally.append(B2)
        Ally.append(B3)
        Ally.append(B4)
        Ally.append(B5)
        Ally.append(B6)

        #Enemy
        Enemy = []

        C0 = [650, 441]
        C1 = [750, 441]
        C2 = [850, 441]
        C3 = [950, 441]
        C4 = [1050, 441]
        C5 = [1150, 441]
        C6 = [1250, 441]


        Enemy.append(C0)
        Enemy.append(C1)
        Enemy.append(C2)
        Enemy.append(C3)
        Enemy.append(C4)
        Enemy.append(C5)
        Enemy.append(C6)


        #FACES
        FACE = [965, 278]
        HERO = [965, 760]

        #END
        END = [1407, 511]
 
        #HeroPower Choices
        HPC1 = [827, 538]
        HPC2 = [1080, 538]
        ################ BOARD  ################
        ################ HAND ################

        #MaxCards = 10 | X position + 100 for number of cards
        Hand = []
        H0 = [702 , 921]
        H1 = [750, 921]
        H2 = [800, 921]
        H3 = [850, 921]
        H4 = [900, 921]
        H5 = [950, 921]
        H6 = [1050, 921]
        H7 = [1000, 921]
        H8 = [1050, 921]
        H9 = [1100, 921]

        Hand.append(H0)
        Hand.append(H1)
        Hand.append(H2)
        Hand.append(H3)
        Hand.append(H4)
        Hand.append(H5)
        Hand.append(H6)
        Hand.append(H7)
        Hand.append(H8)
        Hand.append(H9)

        #Drag and drop
        if(message[:1] == "h"):
           #Si on pars de la main
           #On get ce qu'il y a apres
           # et on le place sur le board
           numberInHand = message[1:]
           cardToPlay = Hand[int(numberInHand)]
           whereToPlay = Ally[int(numberInHand)] 
           randomEnemy = Enemy[int(numberInHand)]
           #Clic sur la carte et la deplacer sur le board
           mouse.drag(cardToPlay[0], whereToPlay[0], cardToPlay[1], whereToPlay[1], True, 2)
           mouse.move(randomEnemy[0], randomEnemy[1], True, 2)  
        
            #BattleCry ?
            
        #Si ca part du board
        if(message[:1] == "b"):
            
            #On prendla carte choisie
            chosenCardOnBoard = int(message[1:])
            whereTo = Ally[chosenCardOnBoard]
            ##On clique
            mouse.move(whereTo[0], whereTo[1], True, 2)
            mouse.hold(button="left")
            

        if(message[:1] == "c"):
            numberEnemy = int(message[1:])
            whereTo = Enemy[numberEnemy]
            mouse.move(whereTo[0], whereTo[1], True, 2)
            mouse.release(button="left")
        ################ HAND ################

        if(message == "face"):
            mouse.move(FACE[0], FACE[1], True, 2)
            mouse.release(button="left")
        
        if(message == "end"):
            mouse.release(button="left")
            mouse.move(END[0], END[1], True, 2)
            mouse.click(button="left")
        
        #Gestion Hero Power
        if(message == "hp"):
            #TODO : en fonction d'une cible ou non (fonction targeting)
            mouse.move(HP[0], HP[1], True, 2)
            mouse.hold(button="left")
            mouse.move(FACE[0], FACE[1], True, 2)







